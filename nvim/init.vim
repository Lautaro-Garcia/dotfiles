"Hago plug de los plugins
call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'benekastah/neomake'
Plug 'ervandew/supertab'
Plug 'Yggdroot/indentLine'
Plug 'scrooloose/nerdcommenter'
Plug 'mattn/emmet-vim', {'for': 'html'}
Plug 'leafo/moonscript-vim', {'for': 'moon'}
call plug#end()

syntax on

"Para que el leader sea la coma
let mapleader=","

"Para que des-remarque las búsquedas
map <Leader>z :noh<CR>

"Seteo el conteo de líneas
set nu

"Para que indente los archivos dependiendo de su tipo
filetype plugin indent on
autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4
autocmd FileType c setlocal expandtab shiftwidth=4 softtabstop=4
autocmd FileType ruby setlocal expandtab shiftwidth=2 softtabstop=2
autocmd FileType eruby setlocal expandtab shiftwidth=2 softtabstop=2
autocmd FileType javascript setlocal expandtab shiftwidth=2 softtabstop=2
autocmd FileType html setlocal expandtab shiftwidth=2 softtabstop=2
autocmd FileType sql setlocal expandtab shiftwidth=2 softtabstop=2
autocmd FileType moon setlocal expandtab shiftwidth=2 softtabstop=2

"Para que use true colors
let $NVIM_TUI_ENABLE_TRUE_COLOR=1

"Para que cargue un colorscheme como default
color birds-of-paradise

"Para que no muestre el "insertar"
set noshowmode

"Para tener un toggle para el foldmethod
map <F9> :set foldmethod=indent <CR>

"Mapea el manejo de ventanas a espacio+w
nnoremap <Space>w <C-W>

"Para que borre los espacios en blanco no deseados
autocmd FileType c,cpp,python,ruby,bash autocmd BufWritePre <buffer> :%s/\s\+$//e

"Para que NeoMake chequee los archivos
autocmd! BufWritePost * Neomake

"Seteo los diferentes makers para neomake
let g:neomake_ruby_enabled_makers = ['mri', 'rubocop']
let g:neomake_c_enabled_makers = ['gcc', 'clang_check']
