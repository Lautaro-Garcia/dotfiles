local awful = require('awful')
local wibox = require('wibox')
local widgets = require('widgets')


local status_wiboxes = {}
local taglist_wiboxes = {}
local taglists = {}

for s = 1, screen.count() do
  status_wiboxes[s] = awful.wibox({position='top', screen=s})
  status_wiboxes[s].x = screen[s].geometry.width * .15
  status_wiboxes[s].width = screen[s].geometry.width * .7

  local status_layout = wibox.layout.flex.horizontal()
  status_layout:fit(status_wiboxes[s].width, status_wiboxes[s].height)
  status_layout:add(widgets.clockwidget)
  status_layout:add(widgets.volumewidget)
  status_layout:add(widgets.batwidget)
  status_layout:add(widgets.mpdwidget)
  status_wiboxes[s]:set_widget(status_layout)

  taglist_wiboxes[s] = awful.wibox({position='bottom', screen=s})
  taglist_wiboxes[s].x = screen[s].geometry.width * .39
  taglist_wiboxes[s].width = screen[s].geometry.width * .22
  taglists[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, {}, {font= "Awesome 15"})

  local taglist_layout = wibox.layout.margin()
  taglist_layout:set_left(taglist_wiboxes[s].width * .25)
  taglist_layout:set_right(taglist_wiboxes[s].width * .25)
  taglist_layout:set_widget(taglists[s])
  taglist_wiboxes[s]:set_widget(taglist_layout)


  status_wiboxes[s].y = 5
  taglist_wiboxes[s].y = screen[s].geometry.height - 28
end
