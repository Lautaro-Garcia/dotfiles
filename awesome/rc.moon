---------- Libraries -----------

-- Standard Awesome library
gears = require 'gears'
awful = require 'awful'
rules = require 'awful.rules'
require 'awful.autofocus'

-- Widget and layout library
wibox = require 'wibox'

-- Theme handling library
beautiful = require 'beautiful'

-- Notifcation library
naughty = require 'naughty'

-- New layouts, a set of widgets and utility functions
lain = require 'lain'

-- Network widgets
netWidgets = require 'net_widgets'


---------- Utility functions -----------

-- Function to join a table of tables
join = (table) ->
  joinedTable = {}
  for x in *table
    joinedTable = awful.util.table.join(joinedTable, x)
  joinedTable
  
-- Function to create vi-like directional movement
viLikeDirectionalMovement = (direction) ->
  awful.client.focus.bydirection(direction)
  client.focus\raise! if client.focus

-- Function to create vi-like directional swap
viLikeDirectionalSwap = (direction) ->
  awful.client.swap.bydirection(direction)
  client.focus\raise! if client.focus

-- Function to notify an error has ocurred
notifyError = (title, text, preset=naughty.config.presets.critical) ->
  notification =
    preset: preset
    title: title
    text: text
  naughty.notify(notification)

-- Function to compute the length of a table
tableLength = (table) ->
  count = 0
  for _ in *table do count += 1
  count

-- Function to add each widget with intermitent background
widgetBackgoundToggle = true
toggleWidgetsAdd = (layout, arrowl_ld, arrowl_dl, ...) ->
  arg = {...}
  if widgetBackgoundToggle
    layout\add(arrowl_ld)
    for i, n in pairs(arg)
      layout\add(wibox.widget.background(n, beautiful.bg_focus))
  else
    layout\add(arrowl_dl)
    for i, n in pairs(arg)
      layout\add(n)
  widgetBackgoundToggle = not widgetBackgoundToggle


---------- Error handling -----------

-- Check if awesome encuntered an error during startup and fall back
-- to another config
if awesome.startup_errors
  notifyError('¡Hubieron algunos errores al iniciar!', awesome.startup_errors)

-- Handle runtime errors after startup
do
  in_error = false
  awesome.connect_signal('debug::error', (err) ->
    return if in_error -- Make sure we don't go into an endless error loop
    in_error = true
    notifyError('Tocaste algo y la cagaste', err)
    in_error = false)


---------- Autostart -----------

os.execute('setxkbmap latam')


---------- Variables definition -----------

terminal   = 'gnome-terminal'
browser    = os.getenv('BROWSER') or 'firefox'
editor     = os.getenv('EDITOR') or 'nvim'
editor_cmd = '#{terminal} -e #{editor}'
modkey     = 'Mod4'
musicplr   = terminal .. " -g 130x34-320+16 -e ncmpcpp"
-- This thing is reeeeally long!
rofiCmd    = 'rofi -show run -color-normal "#222222, #c1c1c1, #222222, #394249, #ffffff"
                             -color-urgent "#222222, #ff1844, #222222, #394249, #ff1844"
                             -color-active "#222222, #80cbc4, #222222, #394249, #80cbc4"
                             -color-window "#222222, #ffffff, #ffffff"
                             -separator-style dash
                             -bw 3'

-- Variables for the termfair layout
lain.layout.termfair.nmaster   = 3
lain.layout.termfair.ncol      = 1

-- Table of layouts to cover with awful.layout.inc, order matters
layouts = {
  lain.layout.centerwork
  lain.layout.termfair
}


---------- Theme -----------

-- Themes define colours, icons and wallpapers
beautiful.init(os.getenv('HOME') .. '/.config/awesome/themes/hybrid/theme.lua')

-- Wallpapaeer
if beautiful.wallpaper
  for s = 1, screen.count!
    gears.wallpaper.maximized(beautiful.wallpaper, s, true)


---------- Tags -----------

-- Defines a tag table which hold all screen tags
tags =
  names: {'I', 'II', 'III', 'IV', 'V'}
  layouts: {layouts[1], layouts[2], layouts[1], layouts[1], layouts[1]}
  mwfact: .54

for s = 1, screen.count!
  tags[s] = awful.tag(tags.names, s, tags.layouts)


---------- Menu -----------

-- Create a launcher widget and a main menu
myAwesomeMenu = {
  {'manual',      terminal .. ' -e man awesome'}
  {'edit config', editor_cmd .. " " .. awesome.conffile}
  {'restart',     awesome.restart}
  {'quit',        awesome.quit}
}

-- Taglist
taglist =
  buttons: join({
    awful.util.table.join awful.button({}, 1, awful.tag.viewonly)
    awful.button({modkey,},                1, awful.client.movetotag)
    awful.button({},                       3, awful.tag.viewtoggle)
    awful.button({modkey,},                3, awful.client.toggletag)
    awful.button({},                       4, (aTag) -> awful.tag.viewnext(aTag.screen))
    awful.button({},                       5, (aTag) -> awful.tag.viewprev(aTag.screen))
  })

-- Text clock widget
myTextClockWidget = awful.widget.textclock!

-- Wibox
myWibox = {}

-- Layout box
myLayoutBox = {}

-- Separators
separators = lain.util.separators
spr  = wibox.widget.textbox(' ')
arrowl = wibox.widget.imagebox!
arrowl\set_image(beautiful.arrl)
arrowl_dl = separators.arrow_left(beautiful.bg_focus, 'alpha')
arrowl_ld = separators.arrow_left('alpha', beautiful.bg_focus)

-- Battery indicator
baticon = wibox.widget.imagebox(beautiful.widget_battery)
batwidget = lain.widgets.bat({
  battery: 'BAT1'
  settings: ->
    if bat_now.perc == 'N/A'
      widget\set_markup(' AC ')
      baticon\set_image(beautiful.widget_ac)
      return
    elseif tonumber(bat_now.perc) <= 5
      baticon\set_image(beautiful.widget_battery_empty)
    elseif tonumber(bat_now.perc) <= 15
      baticon\set_image(beautiful.widget_battery_low)
    else
      baticon\set_image(beautiful.widget_battery)
    widget\set_markup(' ' .. bat_now.perc .. '% ')})

-- Volume indicator
volicon = wibox.widget.imagebox(beautiful.widget_vol)
volumewidget = lain.widgets.alsa(
  settings: ->
    if volume_now.status == "off"
      volicon\set_image(beautiful.widget_vol_mute)
    elseif tonumber(volume_now.level) == 0
      volicon\set_image(beautiful.widget_vol_no)
    elseif tonumber(volume_now.level) <= 50
      volicon\set_image(beautiful.widget_vol_low)
    else
      volicon\set_image(beautiful.widget_vol)
      
    widget\set_text(" " .. volume_now.level .. "% "))

-- MPD widget
mpdicon = wibox.widget.imagebox(beautiful.widget_music)
mpdicon\buttons(awful.util.table.join(awful.button({ }, 1, -> awful.util.spawn_with_shell(musicplr))))
title = ''
artist = ''
mpdwidget = lain.widgets.mpd(
  settings: ->
    if mpd_now.state == 'play'
      artist = ' ' .. mpd_now.artist .. ' '
      title  = mpd_now.title  .. ' '
      mpdicon\set_image(beautiful.widget_music_on)
    elseif mpd_now.state == 'pause' then
        artist = ' mpd '
        title  = 'paused '
    else
        artist = ''
        title  = ''
        mpdicon\set_image(beautiful.widget_music)

    widget\set_markup(lain.util.markup('#EA6F81', artist) .. title))

-- Wireless widget
wirelessWidget = netWidgets.wireless(interface: 'wlp9s0')

-- Wired network widget 
wiredNetWidget = netWidgets.indicator(interfaces: {'enp8s0'})

for s= 1, screen.count! do
  -- Create an icon to show the current layout
  myLayoutBox[s] = awful.widget.layoutbox(s)
  myLayoutBox[s]\buttons(join({
    awful.button({}, 1, -> awful.layout.inc(layouts, 1))
    awful.button({}, 3, -> awful.layout.inc(layouts, -1))
    awful.button({}, 4, -> awful.layout.inc(layouts, 1))
    awful.button({}, 5, -> awful.layout.inc(layouts, -1))
  }))

  -- Create a tag list
  taglist[s] = awful.widget.taglist s,
                                    awful.widget.taglist.filter.all,
                                    taglist.buttons

  -- Create the wibox
  myWibox[s] = awful.wibox(position: top, screen: s)

  -- Widgets that are aligned to the left
  leftLayout = wibox.layout.fixed.horizontal!
  leftLayout\add(taglist[s])

  -- Widgets that are aligned to the right
  right_layout = wibox.layout.fixed.horizontal!
  right_layout\add(wibox.widget.systray!) if s == 1
  right_layout\add(arrowl)
  toggleWidgetsAdd(right_layout, arrowl_ld, arrowl_dl, mpdicon, mpdwidget)
  toggleWidgetsAdd(right_layout, arrowl_ld, arrowl_dl, wirelessWidget)
  toggleWidgetsAdd(right_layout, arrowl_ld, arrowl_dl, wiredNetWidget)
  toggleWidgetsAdd(right_layout, arrowl_ld, arrowl_dl, volicon, volumewidget)
  toggleWidgetsAdd(right_layout, arrowl_ld, arrowl_dl, baticon, batwidget)
  toggleWidgetsAdd(right_layout, arrowl_ld, arrowl_dl, myTextClockWidget, spr)
  toggleWidgetsAdd(right_layout, arrowl_ld, arrowl_dl, myLayoutBox[s])

  -- Bring it all together
  layout = wibox.layout.align.horizontal!
  layout\set_left(leftLayout)
  layout\set_right(right_layout)

  myWibox[s]\set_widget(layout)


---------- Shortcuts -----------

-- Mouse bindings
root.buttons(awful.button({}), 3, -> myAwesomeMenu\toggle())

-- Global key bindings
globalkeys = join({
  -- Switch master client
  awful.key({modkey, 'Shift'}, 'o', ->
    awful.client.swap.byidx(1)
    client.focus\raise! if client.focus)

  -- Layout manipulation
  awful.key({modkey,},         'space', -> awful.layout.inc(layouts, 1))
  awful.key({modkey, 'Shift'}, 'space', -> awful.layout.inc(layouts, -1))

  -- Standard program
  awful.key({modkey, 'Shift'}, 'q',      awesome.quit),
  awful.key({modkey,},         'Return', -> awful.util.spawn(terminal))
  awful.key({modkey, 'Shift'}, 'r',      awesome.restart)

  -- Media keys
  awful.key({}, 'XF86AudioMute',        ->
    os.execute(string.format("amixer set %s toggle", volumewidget.channel))
    volumewidget.update!)
  awful.key({}, 'XF86AudioRaiseVolume', ->
    os.execute(string.format("amixer set %s 1%%+", volumewidget.channel))
    volumewidget.update!)
  awful.key({}, 'XF86AudioLowerVolume', ->
    os.execute(string.format("amixer set %s 1%%-", volumewidget.channel))
    volumewidget.update!)

  -- Shortcuts
  awful.key({'Ctrl'},          'space', -> awful.util.spawn(rofiCmd))
})


-- Client key bindings

clientkeys = join({
  awful.key({modkey,}, 'c', (aClient) -> aClient\kill!)
  awful.key({modkey,}, 't', awful.client.floating.toggle)

  -- Vi-like directional focus
  awful.key({modkey,}, 'j', -> viLikeDirectionalMovement('down'))
  awful.key({modkey,}, 'k', -> viLikeDirectionalMovement('up'))
  awful.key({modkey,}, 'h', -> viLikeDirectionalMovement('left'))
  awful.key({modkey,}, 'l', -> viLikeDirectionalMovement('right'))

  -- Vi-like directional swap
  awful.key({modkey, 'Shift'}, 'j', -> viLikeDirectionalSwap('down'))
  awful.key({modkey, 'Shift'}, 'k', -> viLikeDirectionalSwap('up'))
  awful.key({modkey, 'Shift'}, 'h', -> viLikeDirectionalSwap('left'))
  awful.key({modkey, 'Shift'}, 'l', -> viLikeDirectionalSwap('right'))

  -- Toggle maximize
  awful.key({modkey,}, 'm', (aClient) ->
    aClient.maximized_horizontal = not aClient.maximized_horizontal
    aClient.maximized_vertical = not aClient.maximized_vertical)
})

-- Calculate how many buttons do I need for the workspaces hotkey
keyNumber = 0
for s = 1, screen.count()
  keyNumber = math.min(9, math.max(#tags[s], keyNumber))

-- Numeric workspaces key bindings
for i = 1, keyNumber
  globalkeys = join({
    globalkeys
    awful.key({modkey,}, '#' .. i + 9, ->
      screen = mouse.screen
      tag = awful.tag.gettags(screen)[i]
      awful.tag.viewonly(tag) if tag)
    awful.key({modkey, 'Shift'}, '#' .. i + 9, ->
      if client.focus and tags[client.focus.screen][1]
        awful.client.movetotag(tags[client.focus.screen][i]))
  })

-- Client mouse bindings
clientbuttons = join({
  awful.button({},        1, (aClient) ->
    client.focus = aClient
    aClient\raise!)
  awful.button({modkey,}, 1, awful.mouse.client.move)
  awful.button({modkey,}, 3, awful.mouse.client.resize)
})

-- Set keys
root.keys(globalkeys)


---------- Rules -----------

rules.rules = {
  {rule: {}
   properties: {
     border_width:     beautiful.border_width
     border_color:     beautiful.border_normal
     focus:            awful.client.focus.filter
     raise:            true
     keys:             clientkeys
     buttons:          clientbuttons
     size_hints_honor: false
   }
   callback: awful.client.setslave
  }
  {rule: {class: 'rdesktop'}
   properties: {
     floating:         true
     tag: tags[1][3]
   }
  }
}


---------- Signals -----------

-- Signal function to execute when a new client appears
client.connect_signal('manage', (aClient, startup) ->
  aClient\connect_signal('mouse::enter', (aClient2) ->
    isMagnifierSet = awful.layout.get(aClient2.screen) == awful.layout.suit.magnifier
    if (not isMagnifierSet) and awful.client.focus.filter(aClient2)
      client.focus = aClient2)

  if not startup
    userPositionDefined = aClient.size_hints.user_position
    programPositionDefined = aClient.size_hints.program_position
    if not userPositionDefined and not programPositionDefined
      awful.placement.center_vertical(aClient)
      awful.placement.center_horizontal(aClient))

client.connect_signal('focus', (aClient) ->
  aClient.border_color = beautiful.border_focus)
client.connect_signal('unfocus', (aClient) ->
  aClient.border_color = beautiful.border_normal)

