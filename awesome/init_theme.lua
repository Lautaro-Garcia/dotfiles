local beautiful = require('beautiful')
local gears = require('gears')

beautiful.init("/home/lautaro/.config/awesome/theme/theme.lua")

if beautiful.wallpaper then
  for s = 1, screen.count() do
    gears.wallpaper.maximized(beautiful.wallpaper, s, true)
  end
end
