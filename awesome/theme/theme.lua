theme = {}
theme.font       = "Awesome 10"

theme.grey       = "#63A69B"
theme.yellow     = "#BFC271"
theme.red        = "#A66363"
theme.whitey     = "#C0C5CE"

theme.bg_normal     = "#273941"
theme.bg_focus      = theme.bg_yellow
theme.bg_urgent     = theme.bg_red
theme.bg_minimize   = "#0067ce"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = theme.grey
theme.fg_focus      = theme.whitey
theme.fg_urgent     = "#2e3436"
theme.fg_minimize   = "#2e3436"


theme.border_width  = 2
theme.border_normal = "#456472"
theme.border_focus  = "#63A690"
theme.border_marked = "#eeeeec"

theme.useless_gap_width = 10

-- theme.taglist_squares_sel   = "/usr/share/awesome/themes/default/taglist/squarefw.png"
-- theme.taglist_squares_unsel = "/usr/share/awesome/themes/default/taglist/squarew.png"

theme.wallpaper             = "/home/lautaro/Imágenes/planes.png"

return theme
