local beautiful = require('beautiful')
local awful = require('awful')

client.connect_signal('manage', function(c, startup)
  c:connect_signal('mouse::enter', function (c2)
    local is_magnifier_set = awful.layout.get(c2.screen) == awful.layout.suit.magnifier
    if (not is_magnifier_set) and awful.client.focus.filter(c2) then
      client.focus = c2
    end
  end)
end)
client.connect_signal('focus', function(c) c.border_color = beautiful.border_focus end)
client.connect_signal('unfocus', function(c) c.border_color = beautiful.border_normal end)
