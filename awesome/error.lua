naughty = require('naughty')

-- Function to notify an error has ocurred
local function notifyError (title, text, preset)
  if preset == nil then
    preset = naughty.config.presets.critical
  end
  local notification = {preset = preset,
                        title = title,
                        text = text}
  return naughty.notify(notification)
end

-- Check if awesome encuntered an error during startup and fall back
-- to another config
if(awesome.startup_errors) then
  notifyError('¡Hubieron algunos errores al iniciar!',
              awesome.startup_errors)
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal('debug::error', function (err)
    if(in_error) then return end
    in_error = true
    notifyError('Tocaste algo y la cagaste', err)
    in_error = false
  end)
end
