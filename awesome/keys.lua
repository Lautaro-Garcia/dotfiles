local awful = require('awful')
local layouts = require('layouts')
local tags = require('tags')
local widgets = require('widgets')

local modkey = 'Mod4'

-- This thing is reeeeally long!
local rofiCmd = [[rofi -show run -color-window "#273941, #63a690, #456472"
                                 -color-normal "#273941, #c0c5ce, #273941, #c0c5ce, #273941"
                                 -color-active "#273941, #bfc271, #273941, #273941, #bfc271"
                                 -color-urgent "#273941, #a666363, #273941, #273941, #a666363"
                                 -separator-style solid
                                 -hide-scrollbar
                                 -sidebar-mode]]

local function viLikeDirectionalMovement (direction)
  awful.client.focus.bydirection(direction)
  if client.focus then
    client.focus:raise()
  end
end

local function viLikeDirectionalSwap (direction)
  awful.client.swap.bydirection(direction)
  if client.focus then
    client.focus:raise()
  end
end


local globalkeys = awful.util.table.join(
    -- Layout manipulation
    awful.key({modkey}, 'space', function () awful.layout.inc(layouts, 1) end),
    awful.key({modkey, 'Shift'}, 'space', function () awful.layout.inc(layouts, -1) end),

    -- Standard program
    awful.key({modkey, 'Shift'}, 'q', awesome.quit),
    awful.key({modkey, 'Shift'}, 'r', awesome.restart),

    -- Media keys
    awful.key({}, 'XF86AudioMute', function ()
      os.execute(string.format("amixer set %s toggle", widgets.volumewidget.channel))
      widgets.volumewidget.update()
    end),
    awful.key({}, 'XF86AudioRaiseVolume', function ()
     os.execute(string.format("amixer set %s 1%%+", widgets.volumewidget.channel))
     widgets.volumewidget.update()
    end),
    awful.key({}, 'XF86AudioLowerVolume', function ()
      os.execute(string.format("amixer set %s 1%%-", widgets.volumewidget.channel))
      widgets.volumewidget.update()
    end),

    -- Shortcuts
    awful.key({'Ctrl'}, 'space', function () awful.util.spawn(rofiCmd) end),
    awful.key({modkey}, 'Return', function () awful.util.spawn('gnome-terminal') end)
)

local clientkeys = awful.util.table.join(
    awful.key({modkey}, 'c', function  (c) c:kill() end),

    -- Vi-like directional focus
    awful.key({modkey}, 'j', function () viLikeDirectionalMovement('down') end),
    awful.key({modkey}, 'k', function () viLikeDirectionalMovement('up') end),
    awful.key({modkey}, 'h', function () viLikeDirectionalMovement('left') end),
    awful.key({modkey}, 'l', function () viLikeDirectionalMovement('right') end),

    -- Vi-like directional swap
    awful.key({modkey, 'Shift'}, 'j', function () viLikeDirectionalSwap('down') end),
    awful.key({modkey, 'Shift'}, 'k', function () viLikeDirectionalSwap('up') end),
    awful.key({modkey, 'Shift'}, 'h', function () viLikeDirectionalSwap('left') end),
    awful.key({modkey, 'Shift'}, 'l', function () viLikeDirectionalSwap('right') end),

    -- Toggle maximize
    awful.key({modkey,}, 'm', function (c)
      c.maximized_horizontal = not c.maximized_horizontal
      c.maximized_vertical = not c.maximized_vertical
    end)
)

-- Numeric workspaces key bindings
for i=1, 5 do
  globalkeys = awful.util.table.join(
    globalkeys,
    awful.key({modkey}, '#' .. i + 9, function ()
      local tag = awful.tag.gettags(mouse.screen)[i]
      if tag then
        awful.tag.viewonly(tag)
      end
    end),
    awful.key({modkey, 'Shift'}, '#' .. i + 9, function ()
      if client.focus and tags[client.focus.screen][1] then
        awful.client.movetotag(tags[client.focus.screen][i])
      end
    end)
  )
end

local clientbuttons = awful.util.table.join(
    awful.button({}, 1, function (c)
      client.focus = c
      c:raise()
    end),
    awful.button({modkey}, 1, awful.mouse.client.move),
    awful.button({modkey}, 3, awful.mouse.client.resize)
)

-- Set keys
root.keys(globalkeys)

return {clientkeys=clientkeys, clientbuttons=clientbuttons}
