local layout = require('lain').layout

local layouts = {
  layout.uselessfair,
  layout.uselesstile
}

return layouts
