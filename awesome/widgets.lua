local lain = require('lain')
local awful = require('awful')
local beautiful = require('beautiful')
local fg_color = lain.util.markup.fg.color

return {
  batwidget = lain.widgets.bat({
   battery= 'BAT1',
    settings= function ()
      if bat_now.perc == 'N/A' then
        widget:set_markup(fg_color(beautiful.yellow, '  '))
        return
      elseif tonumber(bat_now.perc) <= 5 then
        icon = ''
      elseif tonumber(bat_now.perc) <= 15 then
        icon = ''
      elseif tonumber(bat_now.perc) <= 50 then
        icon = ''
      elseif tonumber(bat_now.perc) <= 75 then
        icon = ''
      else
        icon = ''
      end
      widget:set_markup(' ' .. fg_color(beautiful.yellow, icon) .. ' ' .. bat_now.perc .. ' ')
    end
  }),

  volumewidget = lain.widgets.alsa({
    settings= function ()
      local icon
      if volume_now.status == 'off' then
        widget:set_text('  ')
        return
      elseif tonumber(volume_now.level) == 0 then
        icon = ''
      elseif tonumber(volume_now.level) <= 50 then
        icon = ''
      else
        icon = ''
      end
      widget:set_text(' ' .. icon .. ' ' .. volume_now.level .. ' ')
    end
  }),

  mpdwidget = lain.widgets.mpd({
    settings= function ()
      local message
      if mpd_now.state == 'play' then
        message = fg_color(beautiful.red, '') .. ' ' .. mpd_now.artist .. ' - ' .. mpd_now.title
      elseif mpd_now.state == 'pause' then
        message = fg_color(beautiful.red, '') .. ' MPD is paused'
      else
        message = fg_color(beautiful.whitey, '') .. ' MPD is not running'
      end

      widget:set_markup(' ' .. message .. ' ')
    end
  }),

  clockwidget= awful.widget.textclock(fg_color(beautiful.whitey, '') .. ' %a %d %b  %H:%M ')
}
