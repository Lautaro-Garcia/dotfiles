local awful = require('awful')
local layouts = require('layouts')

local tag_props = {
  names = {'', '', '', '', ''},
  layouts = {layouts[1], layouts[2], layouts[1], layouts[1], layouts[1]},
  mwfact = .54
}

local tags = {}
for s = 1, screen.count() do
  tags[s] = awful.tag(tag_props.names, s, tag_props.layouts)
end

return tags
